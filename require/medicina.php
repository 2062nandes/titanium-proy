<section id="about-us" class="wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
  <h2 class="wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">Medicina Legal</h2>
  <div class="row">
  <div class="team">
    <div class="center col-xs-12 contenedor">
      <img class="banner-absoluto wow fadeInLeft" data-wow-duration="1500ms" data-wow-delay="600ms" src="img\susponer.png" alt="">
      <div class="cuadro-slide wow fadeInLeft" data-wow-duration="1500ms" data-wow-delay="600ms">
        <h2></h2>
        <p>- Se realiza peritajes de casos de homicidio, asesinato, lesiones, delitos contra la integridad sexual, hechos de transito y diversos temas médico legales.</p>
        <p>- Se realiza peritaje para determinar marca indeleble en rostro.</p>
      </div>
      <img class="banner" src="data1\images\medicina_legal2.jpg" alt="Medicina legal-Titanium">
    </div>
    <div style="text-align: justify;" class="col-xs-12 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
      <p>- Se realiza peritajes de documentos médico legales (certificados forenses, protocolos forenses, historias clínicas, certificados médicos).</p>
      <p>- Se realiza peritajes de exhumación – necropsia (causas de muerte, mecanismos de fallecimiento, data de muerte, colecta de muestras para exámenes periciales).</p>
    </div>
      <div class="col-xs-12 contenedor wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
        <div class="col-xs-4">
          <figure>
            <img class="media-object" style="width: 100%" src="img\perfil\dorian-chavez.jpg" />
          </figure>
        </div>
        <div class="col-xs-8">
          <!-- Description -->
          <h4> DR. DORIAN S. CHÁVEZ ABASTO </h4>
          <p> Especialista en Medicina Legal (CBBA) </p>
          <ul>
            <p>Perito – Consultor técnico en Medicina Legal de casos de homicidio, asesinato, lesiones, delitos contra la integridad sexual.</p>
          </ul>
          <div class="accordion-group">
            <div id="ver-mas-1" class="accordion-body collapse">
              <div class="accordion-inner">
                <ul>
                  <p>Ex Médico Forense del Ministerio Público – Instituto de Investigaciones Forenses Idif del distrito de Cochabamba desde 25 de marzo 2011 al 26 de abril del 2016.</p>
                </ul>
                <p>Actividades de investigación</p>
                <ul>
                  <p>Investigación en Entomología Forense en el Instituto de Ciencia Forense e Investigación Criminal” Icfic – Univalle” Cbba 2007.</p>
                </ul>
              </div>
            </div>
              <div class="col-xs-9">
                <a class="btn btn-primary SeeMore2" data-toggle="collapse" href="#ver-mas-1">Leer más >></a>
                <a class="btn btn-primary" href="docs\curriculum-vitae-dorian-chavez.pdf" download="dorian-chavez-curriculum">Descargar currículum <i class="fa fa-download" aria-hidden="true"></i></a>
              </div>
            <ul class="col-xs-3 center social_icons">
              <li><a href="#"><i class="fa fa-facebook"></i></a></li>
              <li><a href="#"><i class="fa fa-twitter"></i></a></li>
              <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
            </ul>
          </div>
        </div>
      </div>
      <div class="col-xs-12 contenedor wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
        <div class="col-xs-4">
          <figure>
            <img class="media-object" style="width: 100%" src="img\perfil\jorge-martin-melgarejo-pizarroso.jpg" />
          </figure>
        </div>
        <div class="col-xs-8">
            <!-- Description -->
            <h4> DR. JORGE MARTÍN MELGAREJO PIZARROSO</h4>
            <p> Médico Esp. En Medicina Legal. (LPZ) </p>
            <ul>
              <p>Magister Scientiarum en Medicina Forense</p>
              <p>Especialista en Medicina Legal.</p>
              <p>Ex Jefe Nacional de Médicos Forenses del Instituto de Investigaciones Forenses</p>
            </ul>
            <div class="accordion-group">
              <div id="ver-mas-2" class="accordion-body collapse">
                <div class="accordion-inner">
                  <ul>
                    <p>Ex Médico Forense dependiente del Instituto de Investigaciones Forenses Bolivia (2002-2012).</p>
                    <p>Médico Legista del Hospital del Niño” Ovidio Aliaga Uria”. La Paz Bolivia</p>
                    <p>Docente de la Materia de Medicina Legal de la Universisad de Aquino-Bolivia.</p>
                    <p>Docente Invitado De La Maestría De Medicina Legal Universidad Mayor De San Andres.</p>
                    <p>Docente invitado de la Universidad Amazónica de Pando-Bolivia</p>
                    <p>Docente invitado del Post- Grado en “ Auditoria Médica y Gestión de Calidad”. Universidad Siglo XX.</p>
                  </ul>
                </div>
              </div>
                <div class="col-xs-9">
                  <a class="btn btn-primary SeeMore2" data-toggle="collapse" href="#ver-mas-2">Leer más >></a>
                  <a class="btn btn-primary" href="docs\curriculum-vitae-jorge-martin-melgarejo-pizarroso.pdf" download="jorge-martin-melgarejo-pizarroso-curriculum-">Descargar currículum <i class="fa fa-download" aria-hidden="true"></i></a>
                </div>
              <ul class="col-xs-3 center social_icons">
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-xs-12 contenedor wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
          <div class="col-xs-4">
            <figure>
              <img class="media-object" style="width: 100%" src="img\perfil\bismarck-gutierrez.jpg" />
            </figure>
          </div>
          <div class="col-xs-8">
              <!-- Description -->
              <h4> DR. MSC. BISMARCK MILTON GUTIERREZ CALLIZAYA. </h4>
              <p> Médico-Cirujano, Especialista en Medicina Legal</p>
              <ul>
                <p>Encargado de la Morgue Judicial del IDIF-La Paz (2010-2012), Creador del Banco de Datos de Cadáveres “NN” (Ningún Nombre) del IDIF La Paz (2011-2014).</p>
              </ul>
              <div class="accordion-group">
                <div id="ver-mas-3" class="accordion-body collapse">
                  <div class="accordion-inner">
                    <ul>
                      <p>Con Maestría en Medicina Forense, Especialista en Gestión de Calidad en Salud y Auditoria Médica UMSA,</p>
                      <p>Fundador de la División de Tanatología del IDIF-La Paz (2014), Ex Médico Forense del IDIF- La Paz (2010-2014),actualmente Perito en Medicina Forense y Auditoria Médica.</p>
                      <p>Diplomado en Ciencias Forenses UMSA, Diplomado en Educación Superior UMSA, Diplomado en Acupuntura y Microsistemas UMSA, Diplomado en Gestión de Calidad en Salud UMSA,</p>
                      <p>Curso de Criminalística por el IITCUP (Instituto de Investigaciones Técnico Científicas de la Universidad Policial).</p>
                      <p>Curso Internacional Teórico Práctico de Antropología, Genética, Odontología, Entomología y Radiología Forense, EAAF (Equipo Argentino de Antropología Forense) y de la Justicia Nacional de Argentina.</p>
                    </ul>
                  </div>
                </div>
                  <div class="col-xs-9">
                    <a class="btn btn-primary SeeMore2" data-toggle="collapse" href="#ver-mas-3">Leer más >></a>
                    <a class="btn btn-primary" href="docs\curriculum-vitae-bismarck-gutierrez.pdf" download="bismarck-gutierrez-curriculum">Descargar currículum <i class="fa fa-download" aria-hidden="true"></i></a>
                  </div>
                <ul class="col-xs-3 center social_icons">
                  <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                  <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                  <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="col-xs-12 contenedor wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
            <div class="col-xs-4">
              <figure>
                <img class="media-object" style="width: 100%" src="img\perfil\vania-landivar-freire.jpg" />
              </figure>
            </div>
            <div class="col-xs-8">
                <!-- Description -->
                <h4> DRA. VANIA C. LANDIVAR FREIRE. </h4>
                <p>Especialista en Medicina Legal y Forense. España.</p>
                <ul>
                  <p>Especialista en Medicina Legal y Forense otorgado por el Ministerio de Educación, Cultura y Deporte de España. Formada en el Instituto de Medicina Legal de Cataluña (IMLC) en Barcelona-España y la Universidad de Barcelona (UB). </p>
                </ul>
                <div class="accordion-group">
                  <div id="ver-mas-4" class="accordion-body collapse">
                    <div class="accordion-inner">
                      <ul>
                        <p>Magister en Criminalística-Consejero en Ciencias Forenses, con título otorgado por la Universidad Autónoma de Barcelona (UAB).</p>
                        <p>Trabajos de investigación en el ámbito de la medicina legal y forense, a lo largo de su experiencia profesional en España. </p>
                      </ul>
                    </div>
                  </div>
                    <div class="col-xs-9">
                      <a class="btn btn-primary SeeMore2" data-toggle="collapse" href="#ver-mas-4">Leer más >></a>
                    <?php  // <a class="btn btn-primary" href="docs\curriculum-vitae-bismarck-gutierrez.pdf" download="bismarck-gutierrez-curriculum">Descargar currículum <i class="fa fa-download" aria-hidden="true"></i></a> ?>
                    </div>
                  <ul class="col-xs-3 center social_icons">
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
<br><br>
                  </ul>
                </div>
              </div>
            </div>
  </div> <!--/.team -->
  </div><!--section-->
<!--/.container-->
</section><!--/about-us-->
