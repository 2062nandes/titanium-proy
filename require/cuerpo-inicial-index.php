<div class="container container-aux">
  <div class="row">
    <div class="col-xs-12">
      <div class="menu-lateral">
        <!-- FIRST EXAMPLE -->
        <ul id="paneluno">
            <!--  <li><h3>MENU</h3></li>-->
              <li class="animation" style="text-align:center;"><a href="peritajes-consultorias-tecnicas.php">PERITAJES<br>CONSULTORÍAS TÉCNICAS</a></li>
              <li class="animation" style="text-align:center;"><a href="asistencia-tecnica-revision-de-expedientes.php">ASISTENCIA TÉCNICA<br>REVISIÓN DE EXPEDIENTES</a></li>
              <li class="animation" style="text-align:center;"><a href="medicina-legal.php">MEDICINA<br>LEGAL</a></li>
              <li class="animation" style="text-align:center;"><a href="balistica-documentologia-criminalistica.php">BALÍSTICA DOCUMENTOLOGÍA CRIMINALÍSTICA</a></li>
              <ul id="panelthree">
                <li class="mask" style="text-align:center;"><a href="laboratorios-forenses.php">LABORATORIOS<br>FORENSES</a></li>
                <li style="width: 120px; height: 100%;" class="linkOne"><a href="laboratorios-forenses.php#biologia-forense">BIOLOGÍA FORENSE</a></li>
                <li style="width: 120px; height: 100%;" class="linkTwo"><a href="laboratorios-forenses.php#genetica-forense">GENÉTICA FORENSE</a></li>
                <li style="width: 120px; height: 100%;" class="linkThree"><a href="laboratorios-forenses.php#toxicologia-forense">TOXICOLOGÍA FORENSE</a></li>
              </ul>
              <li class="animation" style="text-align:center;"><a href="informatica-forense.php">INFORMÁTICA<br>FORENSE</a></li>
              <ul id="paneltwo">
                  <li class="mask" style="text-align:center;"><a href="otras-especialidades-forenses.php">OTRAS ESPECIALIDADES<br>FORENSES</a></li>
                  <li style="width: 120px; height: 100%;" class="linkOne"><a href="otras-especialidades-forenses.php#psiquiatria-forense">PSIQUIATRÍA FORENSE</a></li>
                  <li style="width: 120px; height: 100%;" class="linkTwo"><a href="otras-especialidades-forenses.php#odontologia-forense">ODONTOLOGÍA FORENSE</a></li>
                  <!-- <li style="width: 120px; height: 100%;" class="linkThree"><a href="otras-especialidades-forenses.php#informatica-forense">INFORMÁTICA FORENSE</a></li> -->
                  <li style="width: 120px; height: 100%;" class="linkThree"><a href="otras-especialidades-forenses.php#patologia-forense">PATOLOGÍA FORENSE</a></li>
                  <li style="width: 120px;" class="linkFour"><a href="otras-especialidades-forenses.php#otorrinolaringologia">OTORRINO - LARINGOLOGÍA</a></li>
                  <li style="width: 120px;" class="linkFive"><a href="otras-especialidades-forenses.php#trabajosocial">TRABAJO<br>SOCIAL</a></li>
              </ul>
              <li class="animation" style="text-align:center;"><a href="asesoria-juridica-forense.php">ASESORÍA <br>JURÍDICA FORENSE</a></li>
        </ul>
      </div>
        <?php //Slider<img src="img/prueba.jpg" width="900" height="400" alt="">
     ?>
       <div class="espacio-menu">
