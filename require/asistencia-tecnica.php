<section id="about-us" class="wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
  <h2 class="wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">Asistencia Técnica - Revisión de Expedientes</h2>
  <div class="row wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
  <div class="team">
    <div class="center col-xs-12 contenedor">
      <img class="banner-absoluto wow fadeInLeft" data-wow-duration="1500ms" data-wow-delay="600ms" src="img\susponer.png" alt="">
      <div class="cuadro-slide wow fadeInLeft" data-wow-duration="1500ms" data-wow-delay="600ms">
        <p>Es un nuevo servicio que se ofrece a solicitud de las partes de forma particular, para orientar sobre los actuados, documentos existentes en un cuaderno de investigación y poder dar una opinión sobre la necesidad de solicitar informes complementarios, ...</p>
      </div>
      <img class="banner" src="data1\images\revision_de_expedientes1.jpg" alt="Revisión de Expedientes-Titanium">
      <!-- <p>a los diferentes Tribunales de Justicia,
      tanto como a los Juzgados, Fiscalías u otras oficinas relacionadas en las materias de cada disciplina profesional
      y en el curso de actuaciones procesales o de investigación de cualquier naturaleza.</p>
    </div> -->
    </div>
    <div style="text-align: justify;" class="col-xs-12 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
      <p>...pericias u observaciones pertinentes y así dar a las partes interesadas una perspectiva para enfocar una estrategia jurídica.</p>
    </div>
  </div>
  </div>
  <br><br><br>
</section>
