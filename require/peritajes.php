<section id="about-us" class="wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
  <h2>Peritajes - Consultorías Técnicas</h2>
  <div class="row">
    <div class="team">
      <div class="center col-xs-12 contenedor">
        <img class="banner-absoluto wow fadeInLeft" data-wow-duration="1500ms" data-wow-delay="600ms" src="img\susponer.png" alt="">
        <div class="cuadro-slide wow fadeInLeft" data-wow-duration="1500ms" data-wow-delay="600ms" >
          <h2></h2>
          <p>Se realizan peritajes en las diferentes áreas forenses que contamos, ofreciendo dictámenes técnicos y profesionales estructurados, fundamentados ,demostrando las operaciones realizadas para el procesado y la resolución de ...</p>
        </div>
        <img class="banner" src="data1\images\peritajes1.jpg" alt="Peritajes-Consultorías Técnicas-Titanium">
      </div>
      <div style="text-align: justify;" class="col-xs-12 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
        <p>...los puntos de pericia planteados y concluyendo en cada uno de los puntos periciales de manera objetiva, científica y clara.</p>
        <p>Consultoría técnica: Ante la complejidad de los diversos temas forenses, la necesidad de una correcta interpretación de informes, dictámenes periciales y la participación de diversos profesionales en juicios orales. Surge la necesidad de un consultor técnico que asista a abogados, fiscales para poder dirigir un correcto interrogatorio en juicio oral a testigos profesionales, peritos. De esta manera  poder  aclarar, concluir en juicio oral las observaciones percibidas y exponerlas a los señores jueces de manera clara para una correcta administración de justicia.</p>
      </div>
    </div>
  </div>
  <br>
</section>
