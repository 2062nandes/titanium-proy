<section id="about-us">
  <h2 class="wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">Balística - Documentología - Criminalística</h2>
  <div class="team wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
   <div id="about-slider" class="wow fadeInDown" data-wow-duration="800ms" data-wow-delay="600ms">
        <div id="carousel-slider" class="carousel slide" data-ride="carousel">
          <!-- Indicators -->
            <ol class="carousel-indicators visible-xs">
              <li data-target="#carousel-slider" data-slide-to="0" class="active"></li>
              <li data-target="#carousel-slider" data-slide-to="1"></li>
              <li data-target="#carousel-slider" data-slide-to="2"></li>
            </ol>

          <div class="carousel-inner">
            <div class="item active">
              <div class="center col-xs-12 contenedor">
                <img class="banner-absoluto wow fadeInLeft" data-wow-duration="1500ms" data-wow-delay="600ms" src="img\susponer.png" alt="">
                <div class="cuadro-slide wow fadeInLeft" data-wow-duration="1500ms" data-wow-delay="600ms">
                  <h2>BALÍSTICA</h2>
                  <p>Es el estudio de los diferentes proyectiles que se utilizan en las armas de fuego.</p>
                  <p>En esta disciplina se analiza el peso, la forma y las dimensiones de una bala.</p>
                </div>
                <img src="data1\images\balisticaforense4.jpg" class="banner" alt="Balística-Titanium">
              </div>
             </div>
             <div class="item">
               <div class="center col-xs-12 contenedor">
                  <img class="banner-absoluto fadeInLeft" src="img\susponer.png" alt="">
                  <div class="cuadro-slide fadeInLeft">
                    <h2>DOCUMENTOLOGÍA</h2>
                    <p>Es la disciplina científica que tiene por objeto de estudio, el análisis de los documentos modernos, públicos o privados, utilizando distintos métodos y técnicas, a fin de establecer su autenticidad o falsedad, plasmando una Pericia Documentológica.</p>
                  </div>
                  <img src="data1\images\documentologia6.jpg" class="banner" alt="Documentología-Titanium">
               </div>
             </div>
             <div class="item">
              <div class="center col-xs-12 contenedor">
                <img class="banner-absoluto fadeInLeft" src="img\susponer.png" alt="">
                 <div class="cuadro-slide fadeInLeft">
                   <h2>CRIMINALÍSTICA</h2>
                   <p>Es una ciencia auxiliar del Derecho Penal cuya actividad principal se centra en descubrir, explicar y probar los delitos que se encuentran bajo investigación.</p>
                 </div>
                <img src="data1\images\criminalistica2.jpg" class="banner" alt="Criminalística-Titanium">
              </div>
            </div>
          </div>
          <a class="left carousel-control hidden-xs" href="#carousel-slider" data-slide="prev">
            <i class="fa fa-angle-left"></i>
          </a>
          <a class=" right carousel-control hidden-xs"href="#carousel-slider" data-slide="next">
            <i class="fa fa-angle-right"></i>
          </a>
        </div> <!--/#carousel-slider-->
  </div><!--/#about-slider-->
</div>
  <div class="row wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
    <div class="team">
      <div class="col-xs-12 contenedor">
        <div class="col-xs-4">
          <figure>
            <img class="media-object" style="width: 100%" src="img\perfil\maribel-doria-medina-pozo.jpg" />
          </figure>
        </div>
        <div class="col-xs-8">
          <!-- Description -->
          <h4> DRA. MARIBEL DORIA MEDINA POZO. </h4>
          <p>Máster en Ciencias Forenses e Investigación Forense</p>
          <p>Área Profesional</p>
          <ul>
            <p>Balística Forense, Huellografia, Documentologia, Criminología.
            <p>Procesamiento de la escena del crimen.</p>
          </ul>
          <div class="accordion-group">
            <div id="ver-mas-1" class="accordion-body collapse">
              <div class="accordion-inner">
                <ul>
                  <p>Ciencias Forenses y Criminalística.</p>
                  <p>Derecho Penal y su Procedimiento.</p>
                  <p>Derecho Civil y Oralidad.</p>
                  <p>Apreciar la complejidad y diversidad de formas en que se constituye, representa y se trata el delito, valorando las teorías sobre el delincuente, la victimización y las respuestas ante el delito y la desviación.</p>
                  <p>Valorar los fundamentos y la diversidad de objetivos de diferentes respuestas ante el delito y la desviación, considerando la protección de los derechos fundamentales.</p>
                  <p>Capacidad para identificar problemas y formular cuestiones criminológicas e investigarlas.</p>
                  <p>Capacidad para analizar, valorar y comunicar información empírica sobre el delito, delincuente, víctima y medios de control social.</p>
                  <p>Capacidad para reconocer problemas éticos que se asocian a la investigación, y llevar a cabo acciones de acuerdo con los respectivos códigos.</p>
                  <p>Capacidad para identificar y aplicar fuentes jurídicas de relevancia en una cuestión criminológica concreta.</p>
                  <p>Comprensión básica de la organización, métodos y estrategias generales de actuación de las instituciones públicas, en relación con la prevención del delito y la respuesta ante el mismo.</p>
                </ul>
                <p>(CBBA) Con capacidad de desplazamiento al interior.</p>
              </div>
            </div>
              <div class="col-xs-9">
                <a class="btn btn-primary SeeMore2" data-toggle="collapse" href="#ver-mas-1">Leer más >></a>
                <!-- <a class="btn btn-primary" href="docs\curriculum-vitae-maribel-doria-medina.pdf" download="maribel-doria-medina-curriculum">Descargar currículum <i class="fa fa-download" aria-hidden="true"></i></a> -->
              </div>
            <ul class="col-xs-3 center social_icons">
              <li><a href="#"><i class="fa fa-facebook"></i></a></li>
              <li><a href="#"><i class="fa fa-twitter"></i></a></li>
              <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
