<section id="contact-page">
        <h2 class="wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">Contáctenos</h2>
        <div class="row contact-wrap wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
            <div class="status alert alert-success" style="display: none; text-align: center;"></div>
            <form class="contact-form" method="post" id="theForm" class="second" action="mail.php" role="form">
                    <div class="col-xs-5 col-xs-offset-1">
                      <div class="form-group">
                        <div class="input">
                          <label for="nombre">Nombre completo:</label>
                          <input class="form-control" type="text" id="nombre" name="nombre" tabindex="1" required>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="input">
                          <label for="telefono">Teléfono:</label>
                          <input class="form-control" type="text" id="telefono" name="telefono" tabindex="2" required>
                        </div>
                      </div>

                      <div class="form-group">
                        <div class="input">
                          <label for="movil">Teléfono móvil:</label>
                          <input class="form-control" type="text" id="movil" name="movil" tabindex="3" required>
                        </div>
                      </div>

                      <div class="form-group">
                        <div class="input">
                          <label for="direccion">Dirección:</label>
                          <input class="form-control" type="text" id="direccion" name="direccion" tabindex="4" required>
                        </div>
                      </div>

                      <div class="form-group">
                        <div class="input">
                          <label for="ciudad">Ciudad:</label>
                          <input class="form-control" type="text" id="ciudad" name="ciudad" tabindex="5" required>
                        </div>
                      </div>
                    </div><!-- fin de colf1 -->
                    <div class="col-xs-5">
                      <div class="form-group">
                        <div class="input">
                          <label for="email">Su e-mail:</label>
                          <input class="form-control" type="email" id="email" name="email" tabindex="6" required>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="especialidad">Seleccione área:</label>
                        <select class="form-control" id="especialidad" type="text" name="especialidad" tabindex="5" required>
                          <option>MEDICINA LEGAL</option>
                          <option>BALÍSTICA – DOCUMENTOLOGIA- CRIMINALÍSTICA.</option>
                          <option>PSIQUIATRIA FORENSE.</option>
                          <option>GENÉTICA FORENSE.</option>
                          <option>ODONTOLOGÍA FORENSE.</option>
                          <option>TOXICOLOGÍA FORENSE.</option>
                        </select>
                      </div>
                      <div class="form-group mensaje">
                        <div class="input">
                          <label for="mensaje">Mensaje:</label>
                          <textarea id="mensaje" class="form-control" cols="55" rows="7" name="mensaje" tabindex="7" rows="8" required></textarea>
                        </div>
                      </div>

                      <div class="form-group botones">
                        <input class="submitbtn btn btn-primary btn-lg" type="submit" tabindex="8" value="Enviar">
                        <input class="deletebtn btn btn-primary btn-lg" type="reset" tabindex="9" value="Borrar">
                      </div>
                    </div><!-- fin de colf2 -->
                    <div class="col-xs-offset-6 col-xs-6">
                      <div id="statusMessage" style="color: white;background: #2e8874; border-left: 7px solid #4e4e4e;"></div>
                    </div>
            </form>
    </div><!--/.row-->
        <h2 style="font-size:45px;" class="wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">Ubíquenos</h2>
        <div class="row wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
          <div class="team">
            <div class="col-xs-6" style="padding-left: 0;padding-right: 1px;">
              <h3>Dirección Cochabamba</h3>
              <iframe src="https://www.google.com/maps/d/embed?mid=1KrpTwqiAofVv377vGBVP8ya7g3c" height="400"></iframe>
            </div>
            <div class="col-xs-6" style="padding-right: 0;padding-left: 1px;">
              <h3>Dirección La Paz</h3>
              <iframe src="https://www.google.com/maps/d/embed?mid=1VR6Xc6f-mwlxZ1KfHttzd0Wtfjc" height="400"></iframe>
            </div>
          </div>
        </div>
</section><!--/#contact-page-->
