<section id="about-us">
  <h2 class="wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">Otras Especialidades</h2>
  <div class="row wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
    <div class="team">
      <h3 id="psiquiatria-forense">Psiquiatría Forense</h3>
      <div class="center col-xs-12 contenedor">
        <img class="banner-absoluto wow fadeInLeft" data-wow-duration="1500ms" data-wow-delay="600ms" src="img\susponer.png" alt="">
        <div class="cuadro-slide wow fadeInLeft" data-wow-duration="1500ms" data-wow-delay="600ms">
          <h2><br></h2>
          <p>Se define como la aplicación de la psiquiatría clínica al derecho, con el objetivo de establecer el estado de las facultades mentales, con el propósito de delimitar el grado de responsabilidad penal y capacidad civil del individuo.</p>
        </div>
        <img class="banner" src="data1\images\psiquiatriaforense2.jpg" alt="Psiquiatría Forense-Titanium">
      </div>
      <div class="col-xs-12 contenedor">
        <div class="col-xs-4">
          <figure>
            <img class="media-object" style="width: 100%" src="img/perfil/claribel-ramirez.jpg" />
          </figure>
        </div>
        <div class="col-xs-8">
            <!-- Description -->
            <h4> DRA. CLARIBEL PATRICIA RAMÍREZ HURTADO </h4>
            <!-- <p> Especialista en medicina Legal Bs. Ar. </p> -->
            <p>(CBBA) Con capacidad de desplazamiento al interior.</p>
            <ul>
              <p>Profesión: Médico</p>
              <p>Especialista en Psiquiatría</p>
              <p>Diplomada en Ciencias Forenses y Policiales</p>
            </ul>
            <div class="accordion-group">
              <div id="ver-mas-1" class="accordion-body collapse">
                <div class="accordion-inner">
                  <ul>
                    <p>Máster en Ciencias Penales y Criminológicas</p>
                    <p>Ex Funcionaria del Instituto de Investigación forense (IDIF)</p>
                    <p>Participó como perito internacional ante la Corte Interamericana de Derechos Humanos(CIDH)</p>
                    <p>Docente  de Pre y Post Grado de la Universidad Mayor de San Andrés, en la materia de Psiquiatría Forense.</p>
                    <p>Autora de libros de Psiquiatría Forense.</p>
                  </ul>
                </div>
              </div>
                <div class="col-xs-9">
                  <a class="btn btn-primary SeeMore2" data-toggle="collapse" href="#ver-mas-1">Leer más >></a>
                  <a class="btn btn-primary" href="docs/curriculum-claribel-ramirez.pdf" download="claribel-ramirez-curriculum">Descargar currículum <i class="fa fa-download" aria-hidden="true"></i></a>
                </div>
              <ul class="col-xs-3 center social_icons">
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
    </div>
  </div>
  <div class="row wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
    <div class="team">
      <h3 id="odontologia-forense">Odontología Forense</h3>
      <div class="center col-xs-12 contenedor">
        <img class="banner-absoluto wow fadeInLeft" data-wow-duration="1500ms" data-wow-delay="600ms" src="img\susponer.png" alt="">
        <div class="cuadro-slide wow fadeInLeft" data-wow-duration="1500ms" data-wow-delay="600ms">
          <h2><br><br></h2>
          <p>Es la aplicación de los conocimientos odontológicos con fines de identificación y tiene utilidad en el derecho Laboral, Civil y Penal.</p>
        </div>
        <img class="banner" src="data1\images\odontologia_forense3.jpg" alt="Odontología Forense-Titanium">
      </div>
      <div class="col-xs-12 contenedor">
        <div class="col-xs-4">
          <figure>
            <img class="media-object" style="width: 100%" src="img\perfil\lourdes-maldonado.jpg" />
          </figure>
        </div>
        <div class="col-xs-8">
          <!-- Description -->
          <h4> DRA. ESP. LOURDES VERÓNICA MALDONADO ANDIA </h4>
          <p> Profesional en Licenciatura en Odontología </p>
          <ul>
            <p>Experiencia laboral en el área de Odontología Forense, Auditoria Odontológica y Odontología clínica, de excelentes relaciones interpersonales y habilidad para trabajar en equipo o individualmente.</p>
          </ul>
          <div class="accordion-group">
            <div id="ver-mas-3" class="accordion-body collapse">
              <div class="accordion-inner">
                <ul>
                  <p>Enfocada en la enseñanza y difusión de la especialidad de Odontología Forense por medio de cursos de postgrado y charlas educativas a nivel nacional.</p>
                  <p>En la consulta privada en la práctica clínica, realizo rehabilitación oral tanto de niños como adultos en diferentes áreas de la odontología.</p>
                  <p>Especialista en Sistemas de Gestión de Calidad ISO:9001 2008.</p>
                  <p>Especialista en Odontología Forense y Auditoria Odontológica</p>
                </ul>
              </div>
            </div>
            <div class="col-xs-9">
              <a class="btn btn-primary SeeMore2" data-toggle="collapse" href="#ver-mas-3">Leer más >></a>
              <a class="btn btn-primary" href="docs\curriculum-vitae-lourdes-maldonado-andia.pdf" download="lourdes-maldonado-andia-curriculum">Descargar currículum <i class="fa fa-download" aria-hidden="true"></i></a>
            </div>
            <ul class="col-xs-3 center social_icons">
              <li><a href="#"><i class="fa fa-facebook"></i></a></li>
              <li><a href="#"><i class="fa fa-twitter"></i></a></li>
              <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
            </ul>
          </div>
        </div>
      </div>
      <div class="col-xs-12 contenedor">
        <div class="col-xs-4">
          <figure>
            <img class="media-object" style="width: 100%" src="img\perfil\benjamin-ramirez.jpg" />
          </figure>
        </div>
        <div class="col-xs-8">
          <!-- Description -->
          <h4> DR. BENJAMIN WLADIMIR RAMÍREZ FLORES </h4>
          <p> Cirujano dentista - UMSA 1996</p>
          <ul>
            <p>Especialista en Implantologia Oral - Especialista en Odontología Forense y Auditoria Odontologíca - Especialista en Gestión de Calidad y auditoria Medica.</p>
          </ul>
          <div class="accordion-group">
            <div id="ver-mas-5" class="accordion-body collapse">
              <div class="accordion-inner">
                <ul>
                  <p>Docente del Diplomado y Maestría de Medicina Forense de la Umsa desde 2004 -2016.</p>
                  <p>Coordinador y Docente de la Especialidad Odontología Forense Universidad Nacional Siglo Xx.</p>
                  <p>Rotación Práctica en Odontología Forense en el Instituto de Medicina Legal y Ciencias Forenses Bogotá Colombia 2004</p>
                  <p>Miembro Fundador y Socio titular De La Sociedad Boliviana de Odontología Forense La Paz.</p>
                  <p>Perito Consultor en Odontología Forense - Valoración de Daño Bucomaxilofacial - Auditoria Odontologíca.</p>
                </ul>
                <p>  Dirección: Edificio Múlticine Piso 6, Oficina 601 <i class="fa fa-phone"></i> 71948153</p>
              </div>
            </div>
            <div class="col-xs-8">
              <a class="btn btn-primary SeeMore2" data-toggle="collapse" href="#ver-mas-5">Leer más >></a>
              <?php /*
              <a class="btn btn-primary" href="docs/curriculum-claribel-ramirez.pdf" download="curriculum">Descargar currículum <i class="fa fa-download" aria-hidden="true"></i></a>
              */ ?>
            </div>
            <ul class="col-xs-3 center social_icons">
              <li><a href="#"><i class="fa fa-facebook"></i></a></li>
              <li><a href="#"><i class="fa fa-twitter"></i></a></li>
              <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php /*
  <div class="row wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
    <div class="team">
      <h3 id="informatica-forense">Informática Forense</h3>
      <div class="center col-xs-12 contenedor">
        <img class="banner-absoluto wow fadeInLeft" data-wow-duration="1500ms" data-wow-delay="600ms" src="img\susponer.png" alt="">
        <div class="cuadro-slide wow fadeInLeft" data-wow-duration="1500ms" data-wow-delay="600ms">
          <h2><br> </h2>
          <p>Se encarga de analizar sistemas informáticos en busca de evidencia que colabore a llevar adelante una causa judicial o una negociación extrajudicial.</p>
        </div>
        <img class="banner" src="data1\images\informatica-forense.jpg" alt="Informática Forense-Titanium">
      </div>
      <div class="col-xs-12 contenedor">
        <div class="col-xs-4">
          <figure>
            <img class="media-object" style="width: 100%" src="img\perfil\verastegui-palao-ivan.jpg" />
          </figure>
        </div>
        <div class="col-xs-8">
            <!-- Description -->
            <h4> ING. IVAN AMADO VERASTEGUI PALAO</h4>
            <p> Ingenierio de Sistemas</p>
            <ul>
              <p>Diplomado en Gobierno Electrónico y sociedad de la información y el conocimiento Ciesi.</p>
              <p>Investigador Informatico Forense Red-Lif.</p>
            </ul>
            <div class="accordion-group">
              <div id="ver-mas-2" class="accordion-body collapse">
                <div class="accordion-inner">
                  <ul>
                    <p>Diplomado en Auditoria de Sistemas- Universidad EMI.</p>
                  </ul>
                  <p>Experiencia en:</p>
                  <ul>
                    <p>Peritaje Informatico Forense - Instituto Americano</p>
                    <p></p>
                  </ul>
                </div>
              </div>
                <div class="col-xs-9">
                  <a class="btn btn-primary SeeMore2" data-toggle="collapse" href="#ver-mas-2">Leer más >></a>
                  <a class="btn btn-primary" href="docs\curriculum-vitae-verastegui-palao-ivan.pdf" download="verastegui-palao-ivan-curriculum">Descargar currículum <i class="fa fa-download" aria-hidden="true"></i></a>
                </div>
              <ul class="col-xs-3 center social_icons">
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
    </div>
  </div>
*/ ?>
  <div class="row wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
    <div class="team">
      <h3 id="patologia-forense">Patología Forense</h3>
      <div class="center col-xs-12 contenedor">
        <img class="banner-absoluto wow fadeInLeft" data-wow-duration="1500ms" data-wow-delay="600ms" src="img\susponer.png" alt="">
        <div class="cuadro-slide wow fadeInLeft" data-wow-duration="1500ms" data-wow-delay="600ms">
          <h2> <br> </h2>
          <p>Se encarga de investigar  la causa y manera de muerte de  personas de interés judicial y responder en forma científica las interrogantes sobre la muerte de personas. Los casos de autopsia obligatoria.</p>
        </div>
        <img class="banner" src="data1\images\patologia-forense.jpg" alt="Patología Forense-Titanium">
      </div>
      <div class="col-xs-12 contenedor">
        <div class="col-xs-4">
          <figure>
            <img class="media-object" style="width: 100%" src="https://x1.xingassets.com/assets/frontend_minified/img/users/nobody_m.original.jpg" />
          </figure>
        </div>
        <div class="col-xs-8">
            <!-- Description -->
            <h4> DR. </h4>
            <p> </p>
            <ul>
              <p></p>
            </ul>
            <div class="accordion-group">
              <div id="ver-mas-4" class="accordion-body collapse">
                <div class="accordion-inner">
                  <ul>
                    <p></p>
                  </ul>
                </div>
              </div>
                <div class="col-xs-9">
                  <a class="btn btn-primary SeeMore2" data-toggle="collapse" href="#ver-mas-4">Leer más >></a>
                  <?php /*
                  <a class="btn btn-primary" href="docs/curriculum-claribel-ramirez.pdf" download="curriculum">Descargar currículum <i class="fa fa-download" aria-hidden="true"></i></a>
                  */ ?>
                </div>
              <ul class="col-xs-3 center social_icons">
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
    </div>
  </div>
<div class="row wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
    <div class="team">
        <h3 id="otorrinolaringologia">Otorrinolaringología</h3>
      <div class="center col-xs-12 contenedor">
          <img class="banner-absoluto wow fadeInLeft" data-wow-duration="1500ms" data-wow-delay="600ms" src="img\susponer.png" alt="">
          <div class="cuadro-slide wow fadeInLeft" data-wow-duration="1500ms" data-wow-delay="600ms">
            <h2> </h2>
            <p>Es la especialidad médica que se encarga del estudio tanto médico como quirúrgico, de las enfermedades del oído, las vías respiratorias superiores y parte de las inferiores, incluyendo nariz, senos paranasales, faringe y laringe.</p>
          </div>
          <img class="banner" src="data1\images\otorrinolaringologia.jpg" alt="Otorrinolaringología">
      </div>
      <div class="col-xs-12 contenedor">
          <div class="col-xs-4">
            <figure>
              <img class="media-object" style="width: 100%" src="img\perfil\raquel-alegre-serrano.jpg" />
            </figure>
          </div>
          <div class="col-xs-8">
              <!-- Description -->
              <h4> DRA. RAQUEL LENNY ALEGRE SERRANO </h4>
              <p>Médico Especialista en Otorrinolaringología (LPZ)</p>
              <ul>
                <p>Licenciada en Medicina y Cirugía</p>
                <p>Entrenamiento médico quirúrgico en Microcirugía de oído en el Hospital Universitario Son Dureta (Palma de Mallorca – España).</p>
              </ul>
              <div class="accordion-group">
                <div id="ver-mas-6" class="accordion-body collapse">
                  <div class="accordion-inner">
                    <ul>
                      <p>Maestría en Medicina Forense</p>
                      <p>Diplomado en Medicina Forense</p>
                      <p>Diplomado en Educación Superior</p>
                      <p>Miembro Titular Sociedad Boliviana de Otorrinolaringología</p>
                      <p>Miembro Titular Sociedad Boliviana de Otorrinolaringología Filial La Paz</p>
                      <p>Miembro de la Interamerican Association of Pediatric Otorhinolaryngology.</p>
                      <p>Miembro de Comité De Auditoría y Expediente Clínico, del Hospital Otorrino-Oftalmológico de la Caja Nacional de Salud, Gestión 2008.</p>
                    </ul>
                  </div>
                </div>
                  <div class="col-xs-9">
                    <a class="btn btn-primary SeeMore2" data-toggle="collapse" href="#ver-mas-6">Leer más >></a>
                    <a class="btn btn-primary" href="docs\curriculum-vitae-raquel-alegre.pdf" download="curriculum-raquel-alegre-serrano">Descargar currículum <i class="fa fa-download" aria-hidden="true"></i></a>
                  </div>
                <ul class="col-xs-3 center social_icons">
                  <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                  <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                  <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
               </ul>
            </div>
        </div>
      </div>
    </div>
</div>
<div class="row wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
    <div class="team">
        <h3 id="trabajosocial">Trabajo Social</h3>
    <div class="center col-xs-12 contenedor">
          <img class="banner-absoluto wow fadeInLeft" data-wow-duration="1500ms" data-wow-delay="600ms" src="img\susponer.png" alt="">
          <div class="cuadro-slide wow fadeInLeft" data-wow-duration="1500ms" data-wow-delay="600ms">
            <h2> <br> <br></h2>
            <p>Intervienen en los procesos judiciales a fin de realizar un dictamen por encargo judicial, en cualquier instancia y en ámbitos como violencia de género, adopción, incapacitación.</p>
          </div>
          <img class="banner" src="data1\images\asesoria_juridica.jpg" alt="Otorrinolaringología">
      </div>
   <div class="col-xs-12 contenedor">
          <div class="col-xs-4">
            <figure>
              <img class="media-object" style="width: 100%" src="img\perfil\jhonny-daza-quispe.jpg" />
            </figure>
          <br>
          </div>
          <div class="col-xs-8">
              <!-- Description -->
              <h4> LIC. JHONNY DAZA QUISPE.</h4>
              <p>Licenciado en Trabajo Social.</p>
              <ul>
                <p>Magister en gestión publica municipal.</p>
                <p>Diplomado en trabajo social forense.</p>
                <p>Diplomado en interculturalidad plurinacional.</p>
              </ul>
              <div class="accordion-group">
                <div id="ver-mas-7" class="accordion-body collapse">
                  <div class="accordion-inner">
                    <ul>
                      <p>Especialidad en macro y micro intervención social.</p>
                      <p>Licenciatura en sociología.</p>
                      <p>Experiencia laboral previa en el IDIF, SEPDAVI.</p>
                    </ul>
                  </div>
                </div>
                  <div class="col-xs-9">
                    <a class="btn btn-primary SeeMore2" data-toggle="collapse" href="#ver-mas-7">Leer más >></a>
                    <!-- <a class="btn btn-primary" href="docs\curriculum-vitae-raquel-alegre.pdf" download="curriculum-raquel-alegre-serrano">Descargar currículum <i class="fa fa-download" aria-hidden="true"></i></a> -->
                  </div>
                <ul class="col-xs-3 center social_icons">
                  <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                  <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                  <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
               </ul>
            </div>
        </div>
      </div>
    </div>
</div>
</section>
