<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="designer" content="Fernando Javier Averanga Aruquipa">
    <meta name="description" content="Peritajes Forense,  Consultorías Técnicas, Asistencia Técnica – Revisión De Expedientes,Medicina Legal,Medicina forense. Consultoría Legal y Asesoría Jurídica Forense. Balística, Documentología, Criminalística. Genética, Odontología y Toxicología Forense">
    <meta name="author" content="">
    <title>TITANIUM | Consultorías forenses, Medicina Legal y Asesoría Jurídica Forense</title>

	<!-- core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Pinyon+Script" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Archivo+Narrow" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
    <?php
    // <link href="css/prettyPhoto.css" rel="stylesheet">
    ?>
  	<link rel="stylesheet" type="text/css" href="engine1/style.css" />
    <link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
    <?php
    // <link rel="shortcut icon" href="img/ico/favicon.ico">
    // <link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/ico/apple-touch-icon-144-precomposed.png">
    // <link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/ico/apple-touch-icon-114-precomposed.png">
    // <link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/ico/apple-touch-icon-72-precomposed.png">
    // <link rel="apple-touch-icon-precomposed" href="img/ico/apple-touch-icon-57-precomposed.png">
    ?>
    <style media="screen">

    /* FIRST EXAMPLE */
    #paneluno {
      width: 17%;
      list-style:none;
      padding: 0;
      margin: 0;
      top: 0;
      padding-top:30px;
      position: absolute;
      display:inline-block;
    }

    #paneluno li {
      border-radius:3px 3px 3px 3px;
      margin-top:5px;
      width: 100%;
      background: #000000;
      background: -moz-linear-gradient(top, #2d2d2d,black,#000000);
      background: -webkit-linear-gradient(top, #2d2d2d,black,#000000);
      background: -o-linear-gradient(top, #2d2d2d,black,#000000);
      border-left:1px solid #111; border-top:1px solid #111; border-right:1px solid #333; border-bottom:1px solid #333;
    }

    #paneluno li.animation {
      -moz-transition: all 0.4s ease-in-out;
      -moz-transform:translateX(0px);
      -o-transition: all 0.4s ease-in-out;
      -o-transform:translateX(0px);
      -webkit-transition: all 0.4s ease-in-out;
      -webkit-transform:translateX(0px);
    }

    #paneluno li.animation:hover {
      -moz-transform:translateX(5px);
      -o-transform:translateX(5px);
      -webkit-transform:translateX(5px);
    }

    #paneluno li a {
      color:#fff;
      display:block;
      padding: 8px 5px;
      box-shadow: -7px 6px 20px #929292;
    }

    #paneluno li a:hover {
      color: #29ffcf;
    }

    /* SECOND EXAMPLE */

    #paneltwo {
      width: 558%;
      list-style: none;
      padding: 0;
      margin: 0;
      display: inline-flex;
      position: relative;
    }

    #paneltwo li {
      border-radius:3px 3px 3px 3px;
      margin-top:5px;
      width: 18%;
      float:left;
      overflow:hidden;
      position:relative;
      background: #000000;
      background: -moz-linear-gradient(top, #2d2d2d,black,#000000);
      background: -webkit-linear-gradient(top, #2d2d2d,black,#000000);
      background: -o-linear-gradient(top, #2d2d2d,black,#000000);
      border-left:1px solid #111; border-top:1px solid #111; border-right:1px solid #333; border-bottom:1px solid #333;
    }

    #paneltwo li.mask { /* It allows us to hide the link behind it */
      z-index:10;
      overflow:hidden;
    }

    #paneltwo li.linkOne, #paneltwo li.linkTwo, #paneltwo li.linkThree, #paneltwo li.linkFour, #paneltwo li.linkFive,#paneltwo li.linkSix
    {   -moz-transition: all 1s ease-in-out;
      -webkit-transition: all 1s ease-in-out;
      -o-transition: all 1s ease-in-out;
    }

    #paneltwo:hover li.linkOne, #paneltwo:hover li.linkTwo, #paneltwo:hover li.linkThree, #paneltwo:hover li.linkFour, #paneltwo:hover li.linkFive, #paneltwo:hover li.linkSix
    { 	-moz-transform:translateX(0px);
      -moz-transition: all 1s ease-in-out;
      -webkit-transform:translateX(0px);
      -webkit-transition: all 1s ease-in-out;
      -o-transform:translateX(0px);
      -o-transition: all 1s ease-in-out;
    }

    #paneltwo li.linkOne {
      -moz-transform:translateX(-90%);
      -moz-transition-delay:0.6s;

      -webkit-transform:translateX(-90%);
      -webkit-transition-delay:0.6s;

      -o-transform:translateX(-90%);
      -o-transition-delay:0.6s;

      z-index:6;
    }
    #paneltwo li.linkTwo {
      -moz-transform:translateX(-180%);
      -moz-transition-delay:0.5s;

      -webkit-transform:translateX(-180%);
      -webkit-transition-delay:0.5s;

      -o-transform:translateX(-180%);
      -o-transition-delay:0.5s;

      z-index:5;
    }
    #paneltwo li.linkThree {
      -moz-transform:translateX(-270%);
      -moz-transition-delay:0.4s;

      -webkit-transform:translateX(-270%);
      -webkit-transition-delay:0.4s;

      -o-transform:translateX(-270%);
      -o-transition-delay:0.4s;

      z-index:4;
    }
    #paneltwo li.linkFour {
      -moz-transform:translateX(-360%);
      -moz-transition-delay:0.3s;

      -webkit-transform:translateX(-360%);
      -webkit-transition-delay:0.3s;

      -o-transform:translateX(-360%);
      -o-transition-delay:0.3s;
      z-index:3;
    }

    #paneltwo li.linkFive {
      -moz-transform:translateX(-450%);
      -moz-transition-delay:0.2s;

      -webkit-transform:translateX(-450%);
      -webkit-transition-delay:0.2s;

      -o-transform:translateX(-450%);
      -o-transition-delay:0.2s;

      z-index:2;
    }
    #paneltwo li.linkSix {
      -moz-transform:translateX(-540%);
      -moz-transition-delay:0.1s;

      -webkit-transform:translateX(-540%);
      -webkit-transition-delay:0.1s;

      -o-transform:translateX(-540%);
      -o-transition-delay:0.1s;

      z-index:1;
    }

    #paneltwo:hover li.linkOne {
      -moz-transition-delay:0s;
      -webkit-transition-delay:0s;
      -o-transition-delay:0s;
      z-index:6;
    }
    #paneltwo:hover li.linkTwo {
      -moz-transition-delay:0.1s;
      -webkit-transition-delay:0.1s;
      -o-transition-delay:0.1s;
      z-index:5;
    }
    #paneltwo:hover li.linkThree {
      -moz-transition-delay:0.2s;
      -webkit-transition-delay:0.2s;
      -o-transition-delay:0.2s;
      z-index:4;
    }
    #paneltwo:hover li.linkFour {
      -moz-transition-delay:0.3s;
      -webkit-transition-delay:0.3s;
      -o-transition-delay:0.3s;
      z-index:3;
    }
    #paneltwo:hover li.linkFive {
      -moz-transition-delay:0.4s;
      -webkit-transition-delay:0.4s;
      -o-transition-delay:0.4s;
      z-index:2;
    }
    #paneltwo:hover li.linkSix {
      -moz-transition-delay:0.5s;
      -webkit-transition-delay:0.5s;
      -o-transition-delay:0.5s;
      z-index:1;
    }
    #paneltwo li a {
      color:#fff;
      display:block;
      padding: 8px;
      box-shadow: -7px 6px 20px #929292;
    }

    #paneltwo li a:hover {
      color: #29ffcf;
    }
    /* TERCER EJEMPLO*/

    #panelthree {
      width: 558%;
      list-style: none;
      padding: 0;
      margin: 0;
      position: relative;
      display: inline-flex;
    }

    #panelthree li {
      border-radius:3px 3px 3px 3px;
      margin-top:5px;
      width: 18%;
      float:left;
      overflow:hidden;
      position:relative;
      background: #000000;
      background: -moz-linear-gradient(top, #2d2d2d,black,#000000);
      background: -webkit-linear-gradient(top, #2d2d2d,black,#000000);
      background: -o-linear-gradient(top, #2d2d2d,black,#000000);
      border-left:1px solid #111; border-top:1px solid #111; border-right:1px solid #333; border-bottom:1px solid #333;
    }

    #panelthree li.mask { /* It allows us to hide the link behind it */
      z-index:10;
      overflow:hidden;
    }

    #panelthree li.linkOne, #panelthree li.linkTwo, #panelthree li.linkThree, #panelthree li.linkFour, #panelthree li.linkFive
    {   -moz-transition: all 1s ease-in-out;
      -webkit-transition: all 1s ease-in-out;
      -o-transition: all 1s ease-in-out;
    }

    #panelthree:hover li.linkOne, #panelthree:hover li.linkTwo, #panelthree:hover li.linkThree, #panelthree:hover li.linkFour, #panelthree:hover li.linkFive
    { 	-moz-transform:translateX(0px);
      -moz-transition: all 1s ease-in-out;
      -webkit-transform:translateX(0px);
      -webkit-transition: all 1s ease-in-out;
      -o-transform:translateX(0px);
      -o-transition: all 1s ease-in-out;
    }

    #panelthree li.linkOne {
      -moz-transform:translateX(-90%);
      -moz-transition-delay:0.6s;

      -webkit-transform:translateX(-90%);
      -webkit-transition-delay:0.6s;

      -o-transform:translateX(-90%);
      -o-transition-delay:0.6s;

      z-index:5;
    }
    #panelthree li.linkTwo {
      -moz-transform:translateX(-180%);
      -moz-transition-delay:0.5s;

      -webkit-transform:translateX(-180%);
      -webkit-transition-delay:0.5s;

      -o-transform:translateX(-180%);
      -o-transition-delay:0.5s;

      z-index:4;
    }
    #panelthree li.linkThree {
      -moz-transform:translateX(-270%);
      -moz-transition-delay:0.4s;

      -webkit-transform:translateX(-270%);
      -webkit-transition-delay:0.4s;

      -o-transform:translateX(-270%);
      -o-transition-delay:0.4s;

      z-index:3;
    }
    #panelthree li.linkFour {
      -moz-transform:translateX(-360%);
      -moz-transition-delay:0.3s;

      -webkit-transform:translateX(-360%);
      -webkit-transition-delay:0.3s;

      -o-transform:translateX(-360%);
      -o-transition-delay:0.3s;
      z-index:2;
    }

    #panelthree li.linkFive {
      -moz-transform:translateX(-450%);
      -moz-transition-delay:0.2s;

      -webkit-transform:translateX(-450%);
      -webkit-transition-delay:0.2s;

      -o-transform:translateX(-450%);
      -o-transition-delay:0.2s;

      z-index:1;
    }

    #panelthree:hover li.linkOne {
      -moz-transition-delay:0s;
      -webkit-transition-delay:0s;
      -o-transition-delay:0s;
      z-index:5;
    }
    #panelthree:hover li.linkTwo {
      -moz-transition-delay:0.1s;
      -webkit-transition-delay:0.1s;
      -o-transition-delay:0.1s;
      z-index:4;
    }
    #panelthree:hover li.linkThree {
      -moz-transition-delay:0.2s;
      -webkit-transition-delay:0.2s;
      -o-transition-delay:0.2s;
      z-index:3;
    }
    #panelthree:hover li.linkFour {
      -moz-transition-delay:0.3s;
      -webkit-transition-delay:0.3s;
      -o-transition-delay:0.3s;
      z-index:2;
    }
    #panelthree:hover li.linkFive {
      -moz-transition-delay:0.4s;
      -webkit-transition-delay:0.4s;
      -o-transition-delay:0.4s;
      z-index:1;
    }
    #panelthree li a {
      color:#fff;
      display:block;
      padding: 8px;
      box-shadow: -7px 6px 20px #929292;
    }

    #panelthree li a:hover {
      color: #29ffcf;
    }
    @-moz-keyframes bounceX {
          0% { -moz-transform: translateX(-205px); -moz-animation-timing-function: ease-in; }

          40% { -moz-transform: translateX(-100px); -moz-animation-timing-function: ease-in; }
          65% { -moz-transform: translateX(-52px); -moz-animation-timing-function: ease-in; }
          82% { -moz-transform: translateX(-25px); -moz-animation-timing-function: ease-in; }
          92% { -moz-transform: translateX(-12px); -moz-animation-timing-function: ease-in; }

          55%, 75%, 87%, 97%, 100% { -moz-transform: translateX(0px); -moz-animation-timing-function: ease-out; }
    }
    </style>
</head><!--/head-->
