<section id="bottom">
    <div class="container wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
        <div class="row">
            <div class="col-xs-4">
                <div class="widget">
                    <h3>Nuestros servicios</h3>
                    <ul>
                        <li><a href="peritajes-consultorias-tecnicas.php">- PERITAJES - CONSULTORÍAS TÉCNICAS</a></li>
                        <li><a href="asistencia-tecnica-revision-de-expedientes.php">- ASISTENCIA TÉCNICA - REVISIÓN DE EXPEDIENTES</a></li>
                        <li><a href="medicina-legal.php">- MEDICINA LEGAL</a></li>
                        <li><a href="balistica-documentologia-criminalistica.php">- BALÍSTICA–DOCUMENTOLOGÍA–CRIMINALÍSTICA</a></li>
                        <li><a href="laboratorios-forenses.php">- LABORATORIOS FORENSES</a></li>
                        <li><a href="otras-especialidades-forenses.php">- OTRAS ESPECIALIDADES FORENSES</a></li>
                        <li><a href="informatica-forense.php">- INFORMÁTICA FORENSE</a></li>
                        <li><a href="asesoria-juridica-forense.php">- ASESORÍA JURÍDICA</a></li>
                    </ul>
                </div>
            </div><!--/.col-md-5-->

            <div class="col-xs-6 center" style="padding-left:0;padding-right:0;">
                <div class="widget">
                  <h3>DIRECCIÓN BOLIVIA</h3>
                  <div id="datos">
                    <p><span style="color:red;font-weight:bold;">COCHABAMBA:</span> Av. Heroínas esq. San Martín, Ed. Golden Tower, P.7 <br>
                      <i style="font-size: 1.2em" class="fa fa-phone"></i>  Telf.: 4505000 <span class="opt"> &nbsp;&nbsp;</span>
                      <i style="font-size: 1.2em" class="fa fa-mobile"> </i> Cel.: 60746099 <span class="opt"> &nbsp;&nbsp;</span>
                      <i style="font-size: .9em" class="fa fa-envelope"></i> titaniumforense@gmail.com <a class="mapita" href="https://www.google.com/maps/d/viewer?mid=1KrpTwqiAofVv377vGBVP8ya7g3c" target="_blank">>>Ver-mapa <i style="font-size: 1.1em" class="fa fa-map-marker"> </i></a></p>
                    <p><span style="color:red;font-weight:bold;">LA PAZ:</span> Av. 6 de Agosto, Ed. Los Jardines, P. 6, Of. E <br>
                      <i style="font-size: 1.2em" class="fa fa-phone"></i>  Telf.: 2444149 <span class="opt"> &nbsp;&nbsp;</span> <i style="font-size: 1.2em" class="fa fa-mobile"></i>Cel.: 70629998 <span class="opt"> &nbsp;&nbsp;</span>
                      <i style="font-size: .9em" class="fa fa-envelope"></i> jorgemelkhart10@gmail.com <a class="mapita" href="https://www.google.com/maps/d/viewer?mid=1VR6Xc6f-mwlxZ1KfHttzd0Wtfjc" target="_blank">>>Ver-mapa <i style="font-size: 1.1em" class="fa fa-map-marker"> </i></a></p>
                    <p><span style="color:red;font-weight:bold;">SANTA CRUZ:</span>Calle Junín 301. 1er piso, Consultorio 104. Zona Centro / Casco Viejo. <br>
                      <i style="font-size: 1.2em" class="fa fa-phone"></i>  Telf.: 3-323274 <span class="opt"> &nbsp;&nbsp;</span>
                      <i style="font-size: .9em" class="fa fa-envelope"></i> jnelson.velmer@gmail.com <a class="mapita" href="javascript:void(0);" target="_blank">>>Ver-mapa <i style="font-size: 1.1em" class="fa fa-map-marker"> </i></a></p>
                  </div>
                </div>
            </div><!--/.col-md-5-->

            <div class="col-xs-2 center">
                <div class="widget">
                  <h3>TITANIUM FORENSE BOLIVIA</h3>
                  <p>Visitenos en: </p>
                  <div class="btn-social">
                          <a class="col-xs-4 btn-youtube" href="#" onclick="void();" target="_blank"><i class="fa fa-google-plus"></i></a>
                          <a class="col-xs-4 btn-twitter" href="#" target="_blank"><i class="fa fa-twitter"></i></a>
                          <a class="col-xs-4 btn-facebook" href="#" target="_blank"><i class="fa fa-facebook"></i></a>
                  </div>
                </div>
            </div><!--/.col-md-2-->
        </div>
    </div>
</section><!--/#bottom-->

<footer id="footer" class="midnight-blue">
    <div class="container">
        <div class="row">
            <div class="col-xs-8" style="font-size: 16px;">
                Todos los Derechos Reservados &REG; <strong style="font-weight:800;">Titanium</strong> &COPY; <?=date("Y");?>.
            </div>
            <div class="col-xs-4" style="color: rgba(255, 255, 255, 0.22);">
                <p style="float:right;">Diseño y Programación <a href="//ahpublic.com" target="_blank">Ah! Publicidad</a></p>
            </div>
        </div>
    </div>
</footer><!--/#footer-->
<div id="ir-arriba">
  <a href="javascript:void(0);"><span><i class="fa fa-angle-double-up"></i></span></a>
</div>

<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<?php
// <script src="js/jquery.prettyPhoto.js"></script>
// <script src="js/jquery.isotope.min.js"></script>
 ?>
<script src="js/main.js"></script>
<script src="js/wow.min.js"></script>
<script type="text/javascript" src="engine1/wowslider.js"></script>
<script type="text/javascript" src="engine1/script.js"></script>
