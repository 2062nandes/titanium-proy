<section id="about-us" class="wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
  <h2 id="asesoria-juridica-forense">Asesoría Jurídica Forense</h2>
  <div class="row wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
    <div class="team">
      <div class="center col-xs-12 contenedor">
        <img class="banner-absoluto wow fadeInLeft" data-wow-duration="1500ms" data-wow-delay="600ms" src="img\susponer.png" alt="">
        <div class="cuadro-slide wow fadeInLeft" data-wow-duration="1500ms" data-wow-delay="600ms">
          <h2> <br> </h2>
          <p>Es aquella que se encarga de ofrecer la información a quien lo necesite para solventar temas relacionados con la aplicación de normativas, leyes y reglamentos, ocupando todas las ramas de la misma.</p>
        </div>
        <img class="banner" src="data1\images\asesoria_juridica.jpg" alt="Asesoría Jurídica Forense-Titanium">
      </div>
      <div class="col-xs-12 contenedor">
        <div class="col-xs-4">
          <figure>
            <img class="media-object" style="width: 100%" src="img\perfil\walter-carlos-torrico.jpg" />
          </figure>
        </div>
        <div class="col-xs-8">
            <!-- Description -->
            <h4> MSC. DR. WALTER CARLOS TORRICO M.</h4>
            <ul>
              <p>Lic. En Ciencias Jurídicas Y Políticas Universidad Mayor de San Simón.</p>
            </ul>
            <p>Post grado:</p>
            <ul>
                <p>Maestría en Ciencias Penales. (UMSS).</p>
            </ul>
            <div class="accordion-group">
              <div id="ver-mas-6" class="accordion-body collapse">
                <div class="accordion-inner">
                  <ul>
                    <p>Maestría en Gerencia Legal en Hidrocarburos. (CESU).</p>
                  </ul>
                </div>
              </div>
                <div class="col-xs-9">
                  <a class="btn btn-primary SeeMore2" data-toggle="collapse" href="#ver-mas-6">Leer más >></a>
                  <?php /*
                  <a class="btn btn-primary" href="docs/curriculum-claribel-ramirez.pdf" download="curriculum">Descargar currículum <i class="fa fa-download" aria-hidden="true"></i></a>
                  */ ?>
                </div>
              <ul class="col-xs-3 center social_icons">
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
    </div>
  </div>
</section><!--/about-us-->
