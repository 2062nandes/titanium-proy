<header id="header">
  <?php /*
    <div class="top-bar">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-xs-4">
                    <div class="top-number"><p><i class="fa fa-phone-square"></i> </p></div>
                </div>
                <div class="col-sm-6 col-xs-8">
                   <div class="social">
                        <ul class="social-share">
                            <li><a href="/">Inicio</a></li>
                            <li><a href="#nosotros.php">Nosotros</a></li>
                            <li><a href="#contacto.php">Contactos</a></li>
                        </ul>
                   </div>
                </div>
            </div>
        </div><!--/.container-->
    </div><!--/.top-bar-->
*/ ?>
    <nav class="navbar navbar-inverse center" role="banner">
      <div class="titanium container">
        <div class="col-xs-4 wow fadeInLeft" data-wow-duration="1000ms" data-wow-delay="600ms">
          <a href="/"><img src="img/titanium.png" alt="Titanium" title="TITANIUM - Servicios Forences">
            <h2>Servicios Forenses</h2>
          </a>
        </div>
        <div class="col-xs-1"></div>
        <div class="col-xs-4 wow fadeInDown" style="width: 31%;padding-left: 0px;" data-wow-duration="1000ms" data-wow-delay="600ms">
          <h3>Trabajando con ciencia <br>y conciencia</h3>
        </div>
        <div class="col-xs-3 wow fadeInRight" style="width: 27%;padding-left: 0px;padding-right: 0px;" data-wow-duration="1000ms" data-wow-delay="600ms">
          <div class="col-xs-12 social" style="float:right;padding-right: 0;">
            <ul class="social-share">
              <li><a href="/">Inicio</a></li>
              <li><a href="nosotros.php">Nosotros</a></li>
              <li><a id="btn-contactos" href="contacto.php">Contactos</a></li>
            </ul>
          </div>
        </div>
      </div>
      <?php /*
        <div class="container">
          <div class="row">
            <div class="col-sm-12">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class=" col-sm-10 collapse navbar-collapse navbar-right">
                <ul class="nav navbar-nav ">
                    <li class="col-sm-offset-3 col-sm-1 active"><a href="#">PERITAJES<br>CONSULTORÍAS<br>TÉCNICAS</a></li>
                    <li class="col-sm-1"><a href="#">ASISTENCIA TÉCNICA <br> REVISIÓN DE EXPEDIENTES</a></li>
                    <li class="col-sm-1"><a href="#">MEDICINA LEGAL</a></li>
                    <li class="col-sm-1"><a href="#">BALÍSTICA – DOCUMENTOLOGÍA<br>CRIMINALÍSTICA</a></li>
                    <li class="col-sm-1" class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">OTRAS ESPECIALIDADES<br>FORENSES<i class="fa fa-angle-down"></i></a>
                        <ul class="dropdown-menu">
                            <li><a href="#">PSIQUIATRA FORENSE</a></li>
                            <li><a href="#">ENÉTICA FORENSE</a></li>
                            <li><a href="#">ODONTOLOGÍA FORENSE</a></li>
                            <li><a href="#">TOXICOLOGÍA FORENSE</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            */ ?>
          </div>
        </div><!--/.container-->
    </nav><!--/nav-->

</header><!--/header-->
