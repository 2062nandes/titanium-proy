<section id="about-us">
  <h2 class="wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">Laboratorios Forenses</h2>
  <div class="row wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
    <!-- <div class="team wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms" style="margin-bottom: 185px;">
      <div class="col-xs-12 contenedorslide">
        <p>La <a style="color:black" href="laboratorios-forenses.php#biologia-forense">Biología forense</a> se ocupa de la colección, identificación y estudio de seres vivos que pueden funcionar como evidencia en materias legales, en especial en casos que llegan a un tribunal de justicia.</p>
        <p>La <a style="color:black" href="laboratorios-forenses.php#genetica-forense">Genética forense</a> es la especialidad que engloba la aplicación de las técnicas de biología molecular utilizando ADN, relacionada con el poder judicial.</p>
        <p>La <a style="color:black" href="laboratorios-forenses.php#toxicologia-forense">Toxicología Forense</a> es la rama de toxicología que estudia los métodos de investigación medico-legal en los casos de envenenamiento y muerte.</p>
      </div>
    </div> -->
    <div class="team">
      <h3 id="biologia-forense">Biología Forense</h3>
      <div class="center col-xs-12 contenedor">
        <img class="banner-absoluto wow fadeInLeft" data-wow-duration="1500ms" data-wow-delay="600ms" src="img\susponer.png" alt="">
        <div class="cuadro-slide wow fadeInLeft" data-wow-duration="1500ms" data-wow-delay="600ms">
          <h2><br></h2>
          <p>Se ocupa de la colección, identificación y estudio de seres vivos que pueden funcionar como evidencia en materias legales, en especial en casos que llegan a un tribunal de justicia.</p>
        </div>
        <img class="banner" src="data1\images\biologia-forense.jpg" alt="Biología Forense-Titanium">
      </div>
      <div class="col-xs-12 contenedor">
        <div class="col-xs-4">
          <figure>
            <img class="media-object" style="width: 100%" src="img\perfil\sergio-emilio-quispe-mayta.jpg" />
          </figure>
        </div>
        <div class="col-xs-8">
            <!-- Description -->
            <h4> DR. SERGIO EMILIO QUISPE MAYTA MS. CS. </h4>
            <p>Servicios de Asesoría en Biología Forense </p>
            <ul>
              <p>-	Casos de Criminalistica. Manchas de sangre, semen, saliva - 	Asesoria en Pericias sobre casos de Violación. </p>
              <p>-	Indicios biológicos ( pelos, uñas, manchas de sangre, manchas de semen, otros) </p>
              <p>-	La prueba de Luminol- Bluestar Forensic. Manchas de sangre lavadas o de data antigua.  </p>
            </ul>
            <div class="accordion-group">
              <div id="ver-mas-1" class="accordion-body collapse">
                <div class="accordion-inner">
                  <p>	Especialidad en Genética Forense y Biología Forense. Universidad Central de Venezuela – Ministerio de Justicia, Santiago de Chile – Universidad Catolica Santiago del Estero, Argentina. </p>
                  <ul>
                    <li><p> Master en Genética y Antropología Forense. Especialidad en Osteología. Especialidad en Genética Humana. Técnicas Antropológicas de Identificación Humana. Criminalística y Paternidad. España, 2008 - 2009</P>
                    <li><p> Diplomado en Ciencias Forenses.Umsa. 2011.</p>
                    <li><p> Diplomado en Ciencias Bioquímicas Forenses.Umsa. 2005</p>
                    <li><p> Especialidad 	“Diagnostico 	Genético - Molecular 	de 	Enfermedades Infecciosas” Caracas – Venezuela. 2004</p>
                    <li><p> Especialidad en Biologia Molecular. Seladis- Umsa. 2003 – 2004</p>
                  </ul>
                </div>
              </div>
                <div class="col-xs-8">
                  <a class="btn btn-primary SeeMore2" data-toggle="collapse" href="#ver-mas-1">Leer más >></a>
                  <a class="btn btn-primary" href="docs\curriculum-vitae-sergio-quispe.pdf" download="sergio-quispe-mayta-curriculum">Descargar currículum <i class="fa fa-download" aria-hidden="true"></i></a>
                  <?php /*
                  <a class="btn btn-primary" href="docs/curriculum-claribel-ramirez.pdf" download="curriculum-claribel-ramirez">Descargar currículum <i class="fa fa-download" aria-hidden="true"></i></a>
                  */ ?>
                </div>
              <ul class="col-xs-4 center social_icons">
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
    </div>
  </div>
  <div class="row wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
    <div class="team">
      <h3 id="genetica-forense">Genética Forense</h3>
      <div class="center col-xs-12 contenedor">
        <img class="banner-absoluto wow fadeInLeft" data-wow-duration="1500ms" data-wow-delay="600ms" src="img\susponer.png" alt="">
        <div class="cuadro-slide wow fadeInLeft" data-wow-duration="1500ms" data-wow-delay="600ms">
          <h2><br></h2>
          <p>Es la especialidad que engloba la aplicación de las técnicas de biología molecular utilizando ADN, relacionada con el poder judicial.</p>
        </div>
        <img class="banner" src="data1\images\gentica_forense2.jpg" alt="Genética Forense-Titanium">
      </div>
      <div class="col-xs-12 contenedor">
        <div class="col-xs-4">
          <figure>
            <img class="media-object" style="width: 100%" src="img\perfil\sergio-emilio-quispe-mayta.jpg" />
          </figure>
        </div>
        <div class="col-xs-8">
            <!-- Description -->
            <h4> DR. SERGIO EMILIO QUISPE MAYTA MS. CS. </h4>
            <p> Servicios de Asesoría en Genética  Forense </p>
            <ul>
              <p>-	Casos de criminalística: ADN (asesinatos, robos).</p>
              <p>-	Identificación  genética de personas.</p>
              <p>-	Delitos de agresión sexual.</p>
              <p>-	Parentesco. Paternidad maternidad, hermandad, abuelidad, otros.</p>
            </ul>
            <div class="accordion-group">
              <div id="ver-mas-2" class="accordion-body collapse">
                <div class="accordion-inner">
                  <p>	Especialidad en Genética Forense y Biología Forense. Universidad Central de Venezuela – Ministerio de Justicia, Santiago de Chile – Universidad Catolica Santiago del Estero, Argentina. </p>
                  <ul>
                    <li><p> Master en Genética y Antropología Forense. Especialidad en Osteología. Especialidad en Genética Humana. Técnicas Antropológicas de Identificación Humana. Criminalística y Paternidad. España, 2008 - 2009</P>
                    <li><p> Diplomado en Ciencias Forenses.Umsa. 2011.</p>
                    <li><p> Diplomado en Ciencias Bioquímicas Forenses.Umsa. 2005</p>
                    <li><p> Especialidad 	“Diagnostico 	Genético - Molecular 	de 	Enfermedades Infecciosas” Caracas – Venezuela. 2004</p>
                    <li><p> Especialidad en Biología Molecular. Seladis- Umsa. 2003 – 2004</p>
                  </ul>
                </div>
              </div>
                <div class="col-xs-8">
                  <a class="btn btn-primary SeeMore2" data-toggle="collapse" href="#ver-mas-2">Leer más >></a>
                  <a class="btn btn-primary" href="docs\curriculum-vitae-sergio-quispe.pdf" download="sergio-quispe-mayta-curriculum">Descargar currículum <i class="fa fa-download" aria-hidden="true"></i></a>
                  <?php /*
                  <a class="btn btn-primary" href="docs/curriculum-claribel-ramirez.pdf" download="curriculum">Descargar currículum <i class="fa fa-download" aria-hidden="true"></i></a>
                  */ ?>
                </div>
              <ul class="col-xs-4 center social_icons">
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
    </div>
  </div>
  <div class="row wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
    <div class="team">
      <h3 id="toxicologia-forense">Toxicología Forense</h3>
      <div class="center col-xs-12 contenedor">
        <img class="banner-absoluto wow fadeInLeft" data-wow-duration="1500ms" data-wow-delay="600ms" src="img\susponer.png" alt="">
        <div class="cuadro-slide wow fadeInLeft" data-wow-duration="1500ms" data-wow-delay="600ms">
          <h2><br><br></h2>
          <p>Es la rama de toxicología que estudia los métodos de investigación medico-legal en los casos de envenenamiento y muerte.</p>
        </div>
        <img class="banner" src="data1\images\toxicologia_forense3.jpg" alt="Toxicología Forense-Titanium">
      </div>
      <div class="col-xs-12 contenedor wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
        <div class="col-xs-4">
          <figure>
            <img class="media-object" style="width: 100%" src="img\perfil\veliz-mercado-nelson.jpg" />
          </figure>
        </div>
        <div class="col-xs-8">
            <!-- Description -->
            <h4> DR. NELSON VÉLIZ MERCADO (SCZ)</h4>
            <p>SUB - Especialidad en Toxicología.</p>
            <ul>
              <p>- Certificación del Centro Nacional de Intoxicaciones del Hospital Profesor Alejandro Posadas. El Palomar – Provincia de Buenos Aires / Argentina.</p>
            </ul>
            <div class="accordion-group">
              <div id="ver-mas-3" class="accordion-body collapse">
                <div class="accordion-inner">
                  <ul>
                    <p>- Carrera de Médico Especialista en Toxicología emitido por la Facultad de Medicina de la Universidad de Buenos Aires (UBA).</p>
                  </ul>
                  <p>Especialidad en Emergenciología.</p>
                  <ul>
                      <p>- Certificación de Conclusión de Residencia Médica emitido por C.R.I.D.A.I.</p>
                      <p>- Certificación del Hospital Universitario Japonés (periodo 1 de Marzo 2008 – 28 de Febrero 2011).</p>
                  </ul>
                  <p>Título de Médico Cirujano.</p>
                  <ul>
                    <p>- Título en Provisión Nacional. República de Bolivia. (Convalidación en la República Argentina finalizada en Febrero año 2009).</p>
                    <p>- Diploma Académico. Universidad Católica Boliviana San Pablo. Año Egreso 2007.</p>
                  </ul>
                </div>
              </div>
                <div class="col-xs-9">
                  <a class="btn btn-primary SeeMore2" data-toggle="collapse" href="#ver-mas-3">Leer más >></a>
                  <a class="btn btn-primary" href="docs\curriculum-vitae-veliz-mercado-nelson.pdf" download="veliz-mercado-nelson-curriculum">Descargar currículum <i class="fa fa-download" aria-hidden="true"></i></a>
                </div>
              <ul class="col-xs-3 center social_icons">
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-xs-12 contenedor wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
          <div class="col-xs-4">
            <figure>
              <img class="media-object" style="width: 100%" src="img\perfil\doris-sandra-uria-huaita.jpg" />
            </figure>
<br>
          </div>
          <div class="col-xs-8">
              <!-- Description -->
              <h4> DRA. DORIS SANDRA URÍA HUAITA. </h4>
              <p> Magíster internacional en toxicología.</p>
              <p>Servicios de Asesoría en: Toxicología Forense</p>
              <ul>
                <p>Peritaje en la Investigación de Acelerantes en prendas</p>
                <p>Peritajes en agresiones sexuales.</p>
              </ul>
              <div class="accordion-group">
                <div id="ver-mas-4" class="accordion-body collapse">
                  <div class="accordion-inner">
                    <ul>
                      <p>Peritajes en drogas de abuso (Benzodiacepinas, Cocaína, Marihuana) en diferentes matrices biológicas (Sangre, Orina, Cabello, Vellos púbicos y Vísceras). </p>
                      <p>Peritajes en plaguicidas Organofosforados, Carbamatos, Piretroides en 	diferentes matrices biológicas (Sangre, Cabello, Vellos púbicos, Vísceras y Médula ósea).</p>
                    </ul>
                    <p>Título De: “Magíster Internacional En Toxicología”, Sevilla – España.</p>
                    <p>Título De: “Experto Internacional En Toxicología”, Sevilla – España.</p>
                    <p>Maestría En “Ciencias Farmacéuticas” Con Mención En Toxicología Clínica-Forense.  Facultad de Bioquímica y Farmacia de la UMSS.  Cochabamba  Bolivia.</p>
                    <p>Diplomado En Docencia Universitaria Modelo Innovador, Posgrado  de la UMSS. Cochabamba-Bolivia.</p>
                  </div>
                </div>
                  <div class="col-xs-9">
                    <a class="btn btn-primary SeeMore2" data-toggle="collapse" href="#ver-mas-4">Leer más >></a>
                    <a class="btn btn-primary" href="docs\curriculum-vitae-doris-sandra-uria-huaita.pdf" download="doris-sandra-uria-huaita-curriculum">Descargar currículum <i class="fa fa-download" aria-hidden="true"></i></a>
                  </div>
                <ul class="col-xs-3 center social_icons">
                  <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                  <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                  <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                </ul>
              </div>
            </div>
          </div>
    </div>
  </div>
</section>
