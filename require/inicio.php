<div id="wowslider-container1" class="wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
  <div class="ws_images"><ul>
    <li><img src="data1/images/criminalistica2.jpg" alt="Criminalística" title="Criminalística" id="wows1_0"/></li>
    <li><img src="data1/images/balisticaforense4.jpg" alt="Balística" title="Balística" id="wows1_1"/></li>
    <li><img src="data1/images/documentologia6.jpg" alt="Documentología" title="Documentología" id="wows1_2"/></li>
    <li><img src="data1/images/gentica_forense2.jpg" alt="Genética forense" title="Genética forense" id="wows1_3"/></li>
    <li><img src="data1/images/medicina_legal2.jpg" alt="Medicina legal" title="Medicina legal" id="wows1_4"/></li>
    <li><img src="data1/images/odontologia_forense3.jpg" alt="Odontología forense" title="Odontología forense" id="wows1_5"/></li>
    <li><img src="data1/images/peritajes1.jpg" alt="Peritajes" title="Peritajes" id="wows1_6"/></li>
    <li><img src="data1/images/psiquiatriaforense2.jpg" alt="Psiquiatría forense" title="Psiquiatría forense" id="wows1_7"/></li>
    <li><img src="data1/images/revision_de_expedientes1.jpg" alt="Revisión de expedientes" title="Revisión de expedientes" id="wows1_8"/></li>
    <li><img src="data1/images/toxicologia_forense3.jpg" alt="Toxicología forense" title="Toxicología forense" id="wows1_9"/></li>
    <li><img src="data1/images/biologia-forense.jpg" alt="Biología forense" title="Biología forense" id="wows1_10"/></li>
    <li><img src="data1/images/informatica-forense.jpg" alt="Informática forense" title="Informática forense" id="wows1_11"/></li>
    <li><img src="data1/images/patologia-forense.jpg" alt="Patología forense" title="Patología forense" id="wows1_12"/></li>
    <li><img src="data1/images/otorrinolaringologia.jpg" alt="Otorrinolaringología" title="Otorrinolaringología" id="wows1_13"/></li>
    <li><img src="data1/images/asesoria_juridica.jpg" alt="Asesoría Jurídica Forense" title="Asesoría Jurídica Forense" id="wows1_14"/></li>
  </ul></div>
  <div class="ws_bullets"><div>
    <a href="#" title="Criminalística"><img src="data1/tooltips/criminalistica2.jpg" alt="Criminalística"/>1</a>
    <a href="#" title="Balística"><img src="data1/tooltips/balisticaforense4.jpg" alt="Balística"/>2</a>
    <a href="#" title="Documentología"><img src="data1/tooltips/documentologia6.jpg" alt="Documentología"/>3</a>
    <a href="#" title="Genética forense"><img src="data1/tooltips/gentica_forense2.jpg" alt="Genética forense"/>4</a>
    <a href="#" title="Medicina legal"><img src="data1/tooltips/medicina_legal2.jpg" alt="Medicina legal"/>5</a>
    <a href="#" title="Odontología forense"><img src="data1/tooltips/odontologia_forense3.jpg" alt="Odontología forense"/>6</a>
    <a href="#" title="Peritajes"><img src="data1/tooltips/peritajes1.jpg" alt="Peritajes"/>7</a>
    <a href="#" title="Psiquiatría forense"><img src="data1/tooltips/psiquiatriaforense2.jpg" alt="Psiquiatría forense"/>8</a>
    <a href="#" title="Revisión de expedientes"><img src="data1/tooltips/revision_de_expedientes1.jpg" alt="Revisión de expedientes"/>9</a>
    <a href="#" title="Toxicología forense"><img src="data1/tooltips/toxicologia_forense3.jpg" alt="Toxicología forense"/>10</a>
    <a href="#" title="Biología forense"><img src="data1/tooltips/biologia_forense.jpg" alt="Biología forense"/>11</a>
    <a href="#" title="Informática forense"><img src="data1/tooltips/informatica_forense.jpg" alt="Informática forense"/>12</a>
    <a href="#" title="Patología forense"><img src="data1/tooltips/patologia_forense.jpg" alt="Patología forense"/>13</a>
    <a href="#" title="Otorrinolaringología"><img src="data1/tooltips/otorrinolaringologia.jpg" alt="Otorrinolaringología"/>13</a>
    <a href="#" title="Asesoría Jurídica Forense"><img src="data1/tooltips/asesoria_juridica.jpg" alt="Asesoría Jurídica Forense"/>14</a>
  </div></div>
<div class="ws_shadow"></div>
</div>
<p></p>
<p></p>
