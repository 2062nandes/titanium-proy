<section id="about-us" class="wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
  <h2 id="asesoria-juridica-forense">Informática Forense</h2>
  <div class="row wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
    <div class="team">
      <div class="center col-xs-12 contenedor">
        <img class="banner-absoluto wow fadeInLeft" data-wow-duration="1500ms" data-wow-delay="600ms" src="img\susponer.png" alt="">
        <div class="cuadro-slide wow fadeInLeft" data-wow-duration="1500ms" data-wow-delay="600ms">
          <h2> <br> </h2>
          <p>Se encarga de analizar sistemas informáticos en busca de evidencia que colabore a llevar adelante una causa judicial o una negociación extrajudicial.</p>
        </div>
        <img class="banner" src="data1\images\informatica-forense.jpg" alt="Informática Forense-Titanium">
      </div>
      <div class="col-xs-12 contenedor">
        <div class="col-xs-4">
          <figure>
            <img class="media-object" style="width: 100%" src="img\perfil\verastegui-palao-ivan.jpg" />
          </figure>
        </div>
        <div class="col-xs-8">
          <!-- Description -->
          <h4> ING. IVAN AMADO VERASTEGUI PALAO</h4>
          <p> Ingenierio de Sistemas</p>
          <ul>
            <p>Diplomado en Gobierno Electrónico y sociedad de la información y el conocimiento Ciesi.</p>
            <p>Investigador Informatico Forense Red-Lif.</p>
          </ul>
          <div class="accordion-group">
            <div id="ver-mas-2" class="accordion-body collapse">
              <div class="accordion-inner">
                <ul>
                  <p>Diplomado en Auditoria de Sistemas- Universidad EMI.</p>
                </ul>
                <p>Experiencia en:</p>
                <ul>
                  <p>Peritaje Informatico Forense - Instituto Americano</p>
                  <p></p>
                </ul>
              </div>
            </div>
              <div class="col-xs-9">
                <a class="btn btn-primary SeeMore2" data-toggle="collapse" href="#ver-mas-2">Leer más >></a>
                <a class="btn btn-primary" href="docs\curriculum-vitae-verastegui-palao-ivan.pdf" download="verastegui-palao-ivan-curriculum">Descargar currículum <i class="fa fa-download" aria-hidden="true"></i></a>
              </div>
            <ul class="col-xs-3 center social_icons">
              <li><a href="#"><i class="fa fa-facebook"></i></a></li>
              <li><a href="#"><i class="fa fa-twitter"></i></a></li>
              <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
            </ul>
          </div>
          </div>
        </div>
    </div>
  </div>
</section><!--/about-us-->
