$(document).ready(function(){
	$('#ir-arriba').hide();
	$(function(){
		$(window).scroll(function(){
			if ($(this).scrollTop() > 290) {
					$('#ir-arriba').fadeIn();
			}else{
					$('#ir-arriba').fadeOut();
			}
		})
	});
	$("#ir-arriba span").click(function(){
			$("body,html").animate({
				scrollTop: 0
			}, 900);
			return false;
	})
});

jQuery(function($) {'use strict',

	//#main-slider
	$(function(){
		$('#main-slider.carousel').carousel({
			interval: 8000
		});
	});


	// accordian
	$('.accordion-toggle').on('click', function(){
		$(this).closest('.panel-group').children().each(function(){
		$(this).find('>.panel-heading').removeClass('active');
		 });

	 	$(this).closest('.panel-heading').toggleClass('active');
	});
	$('.SeeMore2').click(function(){
		var $this = $(this);
		$this.toggleClass('SeeMore2');
		if($this.hasClass('SeeMore2')){
			$this.text('Leer más >>');
		} else {
			$this.text('<< Leer menos');
		}
	});

	//Initiat WOW JS
	new WOW().init();
	//
	// // portfolio filter
	// $(window).load(function(){'use strict';
	// 	var $portfolio_selectors = $('.portfolio-filter >li>a');
	// 	var $portfolio = $('.portfolio-items');
	// 	$portfolio.isotope({
	// 		itemSelector : '.portfolio-item',
	// 		layoutMode : 'fitRows'
	// 	});
	//
	// 	$portfolio_selectors.on('click', function(){
	// 		$portfolio_selectors.removeClass('active');
	// 		$(this).addClass('active');
	// 		var selector = $(this).attr('data-filter');
	// 		$portfolio.isotope({ filter: selector });
	// 		return false;
	// 	});
	// });

	/*Formulario de contacto*/
  $(function(){
    $('input, textarea').each(function() {
      $(this).on('focus', function() {
        $(this).parent('.input').addClass('active');
     });
    if($(this).val() != '') $(this).parent('.input').addClass('active');
    });
  });

  var message = $('#statusMessage');
  $('.deletebtn').click(function() {
    message.removeClass("animMessage");
    $("#theForm input").css("box-shadow", "none");
    $("#theForm textarea").css("box-shadow", "none");
    $("#theForm select").css("box-shadow", "none");
    $(".input").removeClass("active");
  });
  $('.submitbtn').click(function() {
    message.removeClass("animMessage");
    $("#theForm input").css("box-shadow", "");
    $("#theForm textarea").css("box-shadow", "");
    $("#theForm select").css("box-shadow", "");
    if($("#theForm")[0].checkValidity()){
      $.post("mail.php", $("#theForm").serialize(), function(response) {
        if (response == "enviado"){
          $("#theForm")[0].reset();
          $(".input").removeClass("active");
          message.html("Su mensaje fue envíado correctamente,<br>le responderemos en breve");
        }
        else{
          message.html("Su mensaje no pudo ser envíado");
        }
        message.addClass("animMessage");
        $("#theForm input").css("box-shadow", "none");
        $("#theForm textarea").css("box-shadow", "none");
        $("#theForm select").css("box-shadow", "none");
      });
      return false;
    }
  });
	// Contact form
/*	var form = $('#main-contact-form');
	form.submit(function(event){
		event.preventDefault();
		var form_status = $('<div class="form_status"></div>');
		$.ajax({
			url: $(this).attr('action'),

			beforeSend: function(){
				form.prepend( form_status.html('<p><i class="fa fa-spinner fa-spin"></i> Se esta enviando el mensaje...</p>').fadeIn() );
			}
		}).done(function(data){
			form_status.html('<p class="text-success">' + data.message + '</p>').delay(3000).fadeOut();
		});
	});
*/

	//goto top
	$('.gototop').click(function(event) {
		event.preventDefault();
		$('html, body').animate({
			scrollTop: $("body").offset().top
		}, 500);
	});

	// //Pretty Photo
	// $("a[rel^='prettyPhoto']").prettyPhoto({
	// 	social_tools: false
	// });
});
